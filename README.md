케빡이
======

[플레이](https://cam900.gitlab.io/Kebbaki/)

원작:

* 개그콘서트 마빡이
* 주전자닷컴 '제닉'
  * http://www.zuzunza.com/myflash/game_detail.html?game_id=6236

출처:

* 유튜브 '합성편집자'
  * https://youtu.be/cJlt4RmC2sg
* 유튜브 '딥웹케인'
  * https://youtu.be/Pl3uHRBMiZs
  * https://youtu.be/FRYaJLWfMnU
  * https://youtu.be/92volEdYcCQ

제작: MATRIX

플레이 방법
===========

* 화면 상단에 나오는 화살표들을 앞에서부터 순서대로 누릅니다.
* 맨 앞의 화살표가 화면 밖으로 사라지면 케인 코버.
* 최코 점수에 도전해보세요!

빌드 방법
=========

준비물:

Git, GameMaker Studio 2 Web 또는 GameMaker Indie 이상 (HTML5 플랫폼 출력 가능)

GameMaker (Studio 2) 버전: IDE 2023.2.1.75, 런타임 2023.2.1.90 이상

Git 클론:

1. 상단의 Clone 버튼을 클릭하고, 아래 나오는 링크 (SSH 또는 HTTPS 경로)를 복사합니다.
2. 터미널 (명령 프롬포트 등)을 열고 소스코드를 내려받을 폴더로 이동하여 git clone (복사한 링크) 명령을 입력합니다.
3. 위 과정이 성공적으로 완료되었으면, 이를 다른 웹호스팅 서버로 복사해서 플레이할 수 있고, 또는 src 폴더 내의 프로젝트 yyp 파일을 GameMaker (Studio 2)로 열어 다른 폴더에 또는 다른 플랫폼용으로 빌드할 수도 있습니다.

본 레포지토리는 GitLab Page 기능을 이용한 방법을 예시로 시연했습니다.

라이선스
========

본 소프트웨어는 [GPL3.0 라이선스](https://gitlab.com/cam900/Kebbaki/-/blob/main/COPYING) 하에 배포되며, 본 소프트웨어에 사용된 각 에셋들의 저작권은 각 원저작자와 각 저작자에게 있습니다. [여기](https://gitlab.com/cam900/Kebbaki/-/blob/main/LICENSE.md)를 참조해주세요.

    Kebbaki
	Copyright (C) 2021-present  cam900 and contributors

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see[https://www.gnu.org/licenses/](https://www.gnu.org/licenses/).
