/*

	Kebbaki
	Copyright (C) 2021-present  cam900 and contributors

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <https://www.gnu.org/licenses/>.

*/

/// @description Initialize
draw_set_font(Font1);
draw_set_color(c_white);
if (!audio_group_is_loaded(audiogroup_default))
{
	audio_group_load(audiogroup_default);
}
draw_sprite(spr_loading,0,0,0);
draw_sprite(spr_bg,0,0,0);
draw_sprite(spr_status1,0,0,0);
draw_sprite(spr_status2,0,0,0);
draw_sprite(spr_status3,0,0,0);
global.bgmstatus = noone;
global.soundloaded = audio_group_is_loaded(audiogroup_default) && audio_sound_is_playable(snd_intro) && audio_sound_is_playable(snd_loop) && audio_sound_is_playable(snd_nani) && audio_sound_is_playable(snd_outro);
global.curr_score = 0;
global.virtual_pad = 0;
global.vpadval = [0,0,0,0];
event_user(0);