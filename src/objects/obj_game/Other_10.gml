/*

	Kebbaki
	Copyright (C) 2021-present  cam900 and contributors

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <https://www.gnu.org/licenses/>.

*/

/// @description Game over
if (gameover == false)
{
	with(obj_vpad)
	{
		instance_destroy();
	}

	gameover = true;
	scr_bgmstop();
	audio_play_sound(snd_outro,99,false);
	instance_create_layer(168,296,"layer_buttons",obj_retry);
	instance_create_layer(352,288,"layer_buttons",obj_zelda);
}