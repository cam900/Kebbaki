/*

	Kebbaki
	Copyright (C) 2021-present  cam900 and contributors

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <https://www.gnu.org/licenses/>.

*/

/// @description Initialize
scr_bgmstop();
scr_bgmplay(snd_loop,true);
randomize();
curr_str = choose("→","←","↑","↓");
strx = 0;
// 어레이 형식: [[sprite index, string index, x, y], [sprite index, string index, x, y], ...]
bgs = [[spr_status1,1,336,209],[spr_status2,2,160,167],[spr_status3,3,320,192]];
bgp = 0;
gameover = false;
temp_index = ((delta_time*30)/1000000);