/*

	Kebbaki
	Copyright (C) 2021-present  cam900 and contributors

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <https://www.gnu.org/licenses/>.

*/

/// @description Insert description here
// You can write your code in this editor

if (mouse_check_button_pressed(mb_left))
{
	prev_mouse_x = mouse_x;
	prev_mouse_y = mouse_y;
}
if (mouse_check_button(mb_left))
{
	if (prev_mouse_x != mouse_x)
	{
		drag_x += (mouse_x - prev_mouse_x);
		prev_mouse_x = mouse_x;
	}
	if (prev_mouse_y != mouse_y)
	{
		drag_y += (mouse_y - prev_mouse_y);
		prev_mouse_y = mouse_y;
	}
}