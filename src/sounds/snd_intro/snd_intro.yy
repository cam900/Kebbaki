{
  "resourceType": "GMSound",
  "resourceVersion": "1.0",
  "name": "snd_intro",
  "audioGroupId": {
    "name": "audiogroup_default",
    "path": "audiogroups/audiogroup_default",
  },
  "bitDepth": 1,
  "bitRate": 512,
  "compression": 0,
  "conversionMode": 0,
  "duration": 17.299976,
  "parent": {
    "name": "Sounds",
    "path": "folders/Sounds.yy",
  },
  "preload": false,
  "sampleRate": 44100,
  "soundFile": "snd_intro.wav",
  "type": 1,
  "volume": 1.0,
}