{
  "resourceType": "GMSound",
  "resourceVersion": "1.0",
  "name": "snd_nani",
  "audioGroupId": {
    "name": "audiogroup_default",
    "path": "audiogroups/audiogroup_default",
  },
  "bitDepth": 1,
  "bitRate": 512,
  "compression": 0,
  "conversionMode": 0,
  "duration": 1.050658,
  "parent": {
    "name": "Sounds",
    "path": "folders/Sounds.yy",
  },
  "preload": false,
  "sampleRate": 22050,
  "soundFile": "snd_nani.wav",
  "type": 0,
  "volume": 0.5,
}