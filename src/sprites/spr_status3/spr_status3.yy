{
  "resourceType": "GMSprite",
  "resourceVersion": "1.0",
  "name": "spr_status3",
  "bbox_bottom": 279,
  "bbox_left": 0,
  "bbox_right": 344,
  "bbox_top": 0,
  "bboxMode": 0,
  "collisionKind": 1,
  "collisionTolerance": 0,
  "DynamicTexturePage": false,
  "edgeFiltering": false,
  "For3D": false,
  "frames": [
    {"resourceType":"GMSpriteFrame","resourceVersion":"1.1","name":"27599ebd-4c3d-48e8-ae0e-a3c72bdf4fb5",},
    {"resourceType":"GMSpriteFrame","resourceVersion":"1.1","name":"d0ee4603-a807-4333-8a01-4d11d524f802",},
    {"resourceType":"GMSpriteFrame","resourceVersion":"1.1","name":"77242ccd-8cb8-4fba-ba0a-0756e3bf052e",},
    {"resourceType":"GMSpriteFrame","resourceVersion":"1.1","name":"da4ef175-6516-4f48-923c-286e38389e3d",},
    {"resourceType":"GMSpriteFrame","resourceVersion":"1.1","name":"a2e88bfd-0d41-44b7-915f-7ba19c417043",},
    {"resourceType":"GMSpriteFrame","resourceVersion":"1.1","name":"5c64d35c-2d0d-4a79-846f-78535e6c4209",},
    {"resourceType":"GMSpriteFrame","resourceVersion":"1.1","name":"bf47f96f-e119-4c18-ac2a-8ccd4984a1d1",},
    {"resourceType":"GMSpriteFrame","resourceVersion":"1.1","name":"cfb049d5-4799-4c2c-a45a-6cad41724002",},
    {"resourceType":"GMSpriteFrame","resourceVersion":"1.1","name":"65553e96-bf06-4914-b342-70f566f1b067",},
  ],
  "gridX": 0,
  "gridY": 0,
  "height": 280,
  "HTile": false,
  "layers": [
    {"resourceType":"GMImageLayer","resourceVersion":"1.0","name":"20cfcdcc-4a8e-4c85-9609-7754ec929d53","blendMode":0,"displayName":"default","isLocked":false,"opacity":100.0,"visible":true,},
  ],
  "nineSlice": null,
  "origin": 7,
  "parent": {
    "name": "game",
    "path": "folders/Sprites/game.yy",
  },
  "preMultiplyAlpha": false,
  "sequence": {
    "resourceType": "GMSequence",
    "resourceVersion": "1.4",
    "name": "spr_status3",
    "autoRecord": true,
    "backdropHeight": 768,
    "backdropImageOpacity": 0.5,
    "backdropImagePath": "",
    "backdropWidth": 1366,
    "backdropXOffset": 0.0,
    "backdropYOffset": 0.0,
    "events": {"resourceType":"KeyframeStore<MessageEventKeyframe>","resourceVersion":"1.0","Keyframes":[],},
    "eventStubScript": null,
    "eventToFunction": {},
    "length": 9.0,
    "lockOrigin": false,
    "moments": {"resourceType":"KeyframeStore<MomentsEventKeyframe>","resourceVersion":"1.0","Keyframes":[],},
    "playback": 1,
    "playbackSpeed": 30.0,
    "playbackSpeedType": 0,
    "showBackdrop": true,
    "showBackdropImage": false,
    "timeUnits": 1,
    "tracks": [
      {"resourceType":"GMSpriteFramesTrack","resourceVersion":"1.0","name":"frames","builtinName":0,"events":[],"inheritsTrackColour":true,"interpolation":1,"isCreationTrack":false,"keyframes":{"resourceType":"KeyframeStore<SpriteFrameKeyframe>","resourceVersion":"1.0","Keyframes":[
            {"resourceType":"Keyframe<SpriteFrameKeyframe>","resourceVersion":"1.0","Channels":{"0":{"resourceType":"SpriteFrameKeyframe","resourceVersion":"1.0","Id":{"name":"27599ebd-4c3d-48e8-ae0e-a3c72bdf4fb5","path":"sprites/spr_status3/spr_status3.yy",},},},"Disabled":false,"id":"0fedf7e7-9df5-4e68-b3ca-2edc0d1d704f","IsCreationKey":false,"Key":0.0,"Length":1.0,"Stretch":false,},
            {"resourceType":"Keyframe<SpriteFrameKeyframe>","resourceVersion":"1.0","Channels":{"0":{"resourceType":"SpriteFrameKeyframe","resourceVersion":"1.0","Id":{"name":"d0ee4603-a807-4333-8a01-4d11d524f802","path":"sprites/spr_status3/spr_status3.yy",},},},"Disabled":false,"id":"ebad036e-2f2f-4237-928f-a7c20e538ed0","IsCreationKey":false,"Key":1.0,"Length":1.0,"Stretch":false,},
            {"resourceType":"Keyframe<SpriteFrameKeyframe>","resourceVersion":"1.0","Channels":{"0":{"resourceType":"SpriteFrameKeyframe","resourceVersion":"1.0","Id":{"name":"77242ccd-8cb8-4fba-ba0a-0756e3bf052e","path":"sprites/spr_status3/spr_status3.yy",},},},"Disabled":false,"id":"251cc742-ef23-436e-9000-c0bb88614a0c","IsCreationKey":false,"Key":2.0,"Length":1.0,"Stretch":false,},
            {"resourceType":"Keyframe<SpriteFrameKeyframe>","resourceVersion":"1.0","Channels":{"0":{"resourceType":"SpriteFrameKeyframe","resourceVersion":"1.0","Id":{"name":"da4ef175-6516-4f48-923c-286e38389e3d","path":"sprites/spr_status3/spr_status3.yy",},},},"Disabled":false,"id":"5889ffae-3837-4c67-baa1-c8fc06fdf099","IsCreationKey":false,"Key":3.0,"Length":1.0,"Stretch":false,},
            {"resourceType":"Keyframe<SpriteFrameKeyframe>","resourceVersion":"1.0","Channels":{"0":{"resourceType":"SpriteFrameKeyframe","resourceVersion":"1.0","Id":{"name":"a2e88bfd-0d41-44b7-915f-7ba19c417043","path":"sprites/spr_status3/spr_status3.yy",},},},"Disabled":false,"id":"5a867211-1fb1-4cba-b2e9-61dae6c1ec5c","IsCreationKey":false,"Key":4.0,"Length":1.0,"Stretch":false,},
            {"resourceType":"Keyframe<SpriteFrameKeyframe>","resourceVersion":"1.0","Channels":{"0":{"resourceType":"SpriteFrameKeyframe","resourceVersion":"1.0","Id":{"name":"5c64d35c-2d0d-4a79-846f-78535e6c4209","path":"sprites/spr_status3/spr_status3.yy",},},},"Disabled":false,"id":"0037d339-235f-4335-b2d2-9d3491ef0346","IsCreationKey":false,"Key":5.0,"Length":1.0,"Stretch":false,},
            {"resourceType":"Keyframe<SpriteFrameKeyframe>","resourceVersion":"1.0","Channels":{"0":{"resourceType":"SpriteFrameKeyframe","resourceVersion":"1.0","Id":{"name":"bf47f96f-e119-4c18-ac2a-8ccd4984a1d1","path":"sprites/spr_status3/spr_status3.yy",},},},"Disabled":false,"id":"44076250-12d1-4733-9863-ea413ce231cf","IsCreationKey":false,"Key":6.0,"Length":1.0,"Stretch":false,},
            {"resourceType":"Keyframe<SpriteFrameKeyframe>","resourceVersion":"1.0","Channels":{"0":{"resourceType":"SpriteFrameKeyframe","resourceVersion":"1.0","Id":{"name":"cfb049d5-4799-4c2c-a45a-6cad41724002","path":"sprites/spr_status3/spr_status3.yy",},},},"Disabled":false,"id":"49507442-c68c-4ab0-8557-a4369fdc21cb","IsCreationKey":false,"Key":7.0,"Length":1.0,"Stretch":false,},
            {"resourceType":"Keyframe<SpriteFrameKeyframe>","resourceVersion":"1.0","Channels":{"0":{"resourceType":"SpriteFrameKeyframe","resourceVersion":"1.0","Id":{"name":"65553e96-bf06-4914-b342-70f566f1b067","path":"sprites/spr_status3/spr_status3.yy",},},},"Disabled":false,"id":"c2593bbf-049b-4fa8-b030-1c1c7a601a24","IsCreationKey":false,"Key":8.0,"Length":1.0,"Stretch":false,},
          ],},"modifiers":[],"spriteId":null,"trackColour":0,"tracks":[],"traits":0,},
    ],
    "visibleRange": null,
    "volume": 1.0,
    "xorigin": 172,
    "yorigin": 280,
  },
  "swatchColours": null,
  "swfPrecision": 2.525,
  "textureGroupId": {
    "name": "Default",
    "path": "texturegroups/Default",
  },
  "type": 0,
  "VTile": false,
  "width": 345,
}