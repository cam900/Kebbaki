/*

	Kebbaki
	Copyright (C) 2021-present  cam900 and contributors

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <https://www.gnu.org/licenses/>.

*/

// Script assets have changed for v2.3.0 see
// https://help.yoyogames.com/hc/en-us/articles/360005277377 for more information
function draw_text_w(_x, _y, _str, _w)
{
	var xx = _x;
	var i = 1;
	var len = string_length(_str);
	while (i <= len)
	{
		draw_text(xx, _y, string_char_at(_str, i));
		i++;
		xx += _w;
	}
}
function draw_text_outline(_x, _y, _str, _outcol)
{
	for (var yy = -1; yy <= 1; yy++)
	{
		for (var xx = -1; xx <= 1; xx++)
		{
			draw_text_color(_x+xx,_y+yy,_str,_outcol,_outcol,_outcol,_outcol,1);
		}
	}
	draw_text(_x,_y,_str);
}
