/*

	Kebbaki
	Copyright (C) 2021-present  cam900 and contributors

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <https://www.gnu.org/licenses/>.

*/

// Script assets have changed for v2.3.0 see
// https://help.yoyogames.com/hc/en-us/articles/360005277377 for more information
function scr_texlist(_ind)
{
	var str = "";
	switch (_ind)
	{
	case 0:
		str = "원작: 주전자닷컴 '제닉'\n출처: 합성편집자, 딥웹케인\n제작: MATRIX";
		break;
	case 1:
		str = "내가 누군지 알어?\n콜목대장 뭉탱이여\n콜목대장 뭉탱이를\n뭘로 보~고\n박수치지마라";
		break;
	case 2:
		str = "헉~~헉\n코 장풍은 의미가 없어\n박수치지 말래니깐";
		break;
	case 3:
		str = "이제 코만좀 눌러대라\n나 죽는다.\n그만 케인코버좀 하지";
		break;
	case 4:
		str = "화면이 제대로 보이지 않는 경우, \n기기 화면 방향을 가로로 설정해주십시오.";
		break;
	case 5:
		str = "Kebbaki\nCopyright (C) 2021-present  cam900 and contributors\n\nThis program is free software: you can redistribute it and/or modify\nit under the terms of the GNU General Public License as published by\nthe Free Software Foundation, either version 3 of the License, or\n(at your option) any later version.\n\nThis program is distributed in the hope that it will be useful,\nbut WITHOUT ANY WARRANTY; without even the implied warranty of\nMERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the\nGNU General Public License for more details.\n\nYou should have received a copy of the GNU General Public License\n\nalong with this program.  If not, see <https://www.gnu.org/licenses/>.";
		break;
	}
	return str;
}
